
#define USE_STDPERIPH_DRIVER
#include "stm32f4xx.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "tm_stm32f4_usart.h"

#define BLOCK_

static void ToggleLED_Timer(void *pvParameters);

static xQueueHandle buttonpress_q; 

//Flash orange LED at about 1hz
int main(void)
{
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;  // enable the clock to GPIOD
    __asm("dsb");                         // stall instruction pipeline, until instruction completes, as
                                          //    per Errata 2.1.13, "Delay after an RCC peripheral clock enabling"
    GPIOD->MODER = (1 << 26);             // set pin 13 to be general purpose output
    //UART_Init();

    /* Initialize USART1 at 9600 baud, TX: PB6, RX: PB7 */
    TM_USART_Init(USART2, TM_USART_PinsPack_1, 115200);
    
    /* Create IPC variables */
    buttonpress_q = xQueueCreate(10, sizeof(int));
    if (buttonpress_q == 0) {
      while(1); /* fatal error */
    }

    xTaskCreate(
        ToggleLED_Timer,                 /* Function pointer */
        "Task1",                          /* Task name - for debugging only*/
        configMINIMAL_STACK_SIZE,         /* Stack depth in words */
        (void*) NULL,                     /* Pointer to tasks arguments (parameter) */
        tskIDLE_PRIORITY + 2UL,           /* Task priority*/
        NULL                              /* Task handle */
    );

    vTaskStartScheduler();

    while(1) {
    }
}


void ToggleLED_Timer(void *pvParameters){

  while (1) {

    GPIOD->ODR ^= (1 << 13);           // Toggle the pin 
    TM_USART_Puts(USART2, "Heppo worpd\n");

    /*
    Delay for a period of time. vTaskDelay() places the task into
    the Blocked state until the period has expired.
    The delay period is spacified in 'ticks'. We can convert
    yhis in milisecond with the constant portTICK_RATE_MS.
    */
    vTaskDelay(1500 / portTICK_RATE_MS);
  }
  (void)pvParameters;
}


